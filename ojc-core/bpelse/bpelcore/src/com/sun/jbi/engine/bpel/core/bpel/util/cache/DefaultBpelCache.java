package com.sun.jbi.engine.bpel.core.bpel.util.cache;

import com.sun.jbi.engine.bpel.core.bpel.util.BpelCache;
import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author David BRASSELY (brasseld at gmail.com)
 */
public class DefaultBpelCache implements BpelCache {
    
    private Map<String, String> cache = new HashMap<String, String>();

    public String put(String key, String value) {
        cache.put(key, value);
        return key;
    }

    public String get(String key) {
        return cache.get(key);
    }

    public String remove(String key) {
        return cache.remove(key);
    }
}
