package com.sun.jbi.engine.bpel.core.bpel.xpath.functions.cache;

import com.sun.jbi.engine.bpel.core.bpel.util.BpelCache;
import com.sun.jbi.engine.bpel.core.bpel.xpath.functions.BPWSFunctions;
import org.apache.commons.jxpath.ExpressionContext;

/**
 *
 * @author David BRASSELY (brasseld at gmail.com)
 */
public class PutCacheFunction extends AbstractCacheFunction {

    public PutCacheFunction(BpelCache bpelCache) {
        super(bpelCache);
    }
        
    public Object invoke(ExpressionContext context, Object[] parameters) {
        System.out.println("Parameters: " + parameters.length);
        System.out.println("Parameter 0: " + parameters[0]);
        System.out.println("Parameter 1: " + parameters[1]);
        return bpelCache.put((String) BPWSFunctions.convertParam(parameters[0]), 
                (String) BPWSFunctions.convertParam(parameters[1]));
    }
}
